<!doctype html>
<html>
  <head>
    <link rel="stylesheet" href="../../../assets/css/default.css" />
    <link rel="stylesheet" href="../../../assets/css/util.css" />
    <link rel="stylesheet" href="../../../assets/css/pygments.css" />
    <link rel="stylesheet" href="../../../assets/css/site.css" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Binding FetchEvent Using Properties and Constructors | webbureaucrat | a blog about ReasonML and other programmery things. </title>
  </head>
  <body>
    <div class="container">
      <h1 class="h1">
        <a href="../../../index.html">webbureaucrat</a>
      </h1>
      <div class="padded">
        <nav class="align-right">
  <a href="/">Home</a>
  <a href="/about.html">About</a>
</nav>

        <h2>Binding FetchEvent Using Properties and Constructors</h2>
        <p>Having successfully bound
<a href="https://developer.mozilla.org/en-US/docs/Web/API/ExtendableEvent">ExtendableEvent</a>, I can now work on inheriting this interface for
<a href="https://developer.mozilla.org/en-US/docs/Web/API/FetchEvent">FetchEvent</a>.
Along the way, I’ll install <a href="https://www.npmjs.com/package/bs-fetch">bs-fetch</a>
as a BuckleScript dependency and bind to JavaScript properties.</p>

<h3 id="installing-bs-fetch">Installing bs-fetch</h3>
<p>I’ve already bound
<a href="https://developer.mozilla.org/en-US/docs/Web/API/ExtendableEvent">ExtendableEvent</a>, 
but according to the
<a href="https://developer.mozilla.org/en-US/docs/Web/API/FetchEvent">FetchEvent</a> 
documentation, I need a couple more types before I can bind FetchEvent. The 
property 
<a href="https://developer.mozilla.org/en-US/docs/Web/API/FetchEvent/preloadResponse">preloadResponse</a> 
depends on the
<a href="https://developer.mozilla.org/en-US/docs/Web/API/Response">Response</a> type, 
and the property 
<a href="https://developer.mozilla.org/en-US/docs/Web/API/FetchEvent/request">request</a>
depends on the 
<a href="https://developer.mozilla.org/en-US/docs/Web/API/Request">Request</a> type. These
types are out of scope for me because I’m trying to implement the 
<a href="https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API">Service Worker Api</a>, whereas <code class="language-plaintext highlighter-rouge">Request</code> and <code class="language-plaintext highlighter-rouge">Response</code> are a part of the
<a href="https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API">Fetch API</a>. 
Fortunately for me, there’s already a binding for the Fetch API in 
<a href="https://www.npmjs.com/package/bs-fetch">bs-fetch</a>. Installation is very
simple.</p>

<ol>
  <li><strong>Run <code class="language-plaintext highlighter-rouge">npm install bs-fetch</code>.</strong></li>
  <li>Add “bs-fetch” to the list of bs-dependencies in the <em>bsconfig.json</em>.</li>
</ol>

<p>After that, we should be able to use bs-fetch.</p>

<h3 id="initializing-the-type-and-binding-to-a-javascript-constructor">Initializing the type and binding to a JavaScript constructor</h3>
<p>I have a controversial hottake in functional programming–I love creating a
bunch of short files. A lot of functional programmers pride themselves in how
little they can fit into a single file, but personally, while I love how 
succinct functional languages are, I still do not like to scroll, so I am going
to start by creating a new file specificlly for this type and call it 
<em>FetchEvent.re</em>.</p>

<p>One benefit of having a bunch of short files is it makes it easier to manage
a bunch of <a href="https://reasonml.github.io/docs/en/module#open-ing-a-module">open</a>
statements without making a mess or creating ambiguities, so I’m going to open
two modules I’m dependent on.</p>

<figure class="highlight"><pre><code class="language-ocaml" data-lang="ocaml"><span class="k">open</span> <span class="nc">ExtendableEvent</span><span class="p">;</span>
<span class="k">open</span> <span class="nc">Fetch</span><span class="p">;</span></code></pre></figure>

<p>Next, I’m going to initialize my new type using the same 
<a href="/2020/06/11/reasonml-journey-part-ii-subtyping-and-extendableevent.html">subtyping technique</a> we used in my first ReasonML series.</p>

<figure class="highlight"><pre><code class="language-ocaml" data-lang="ocaml"><span class="k">type</span> <span class="n">_fetchEvent</span><span class="p">(</span><span class="k">'</span><span class="n">a</span><span class="p">);</span>
<span class="k">type</span> <span class="n">fetchEvent_like</span><span class="p">(</span><span class="k">'</span><span class="n">a</span><span class="p">)</span> <span class="o">=</span> <span class="n">extendableEvent_like</span><span class="p">(</span><span class="n">_fetchEvent</span><span class="p">(</span><span class="k">'</span><span class="n">a</span><span class="p">));</span>
<span class="k">type</span> <span class="n">fetchEvent</span> <span class="o">=</span> <span class="n">fetchEvent_like</span><span class="p">(</span><span class="nn">Dom</span><span class="p">.</span><span class="n">_baseClass</span><span class="p">);</span></code></pre></figure>

<p>This effectively makes <code class="language-plaintext highlighter-rouge">fetchEvent</code> a subtype of <code class="language-plaintext highlighter-rouge">extendableEvent</code>, so it can
use <code class="language-plaintext highlighter-rouge">extendableEvent</code>’s 
<a href="https://developer.mozilla.org/en-US/docs/Web/API/ExtendableEvent/waitUntil">waitUntil</a> method that we defined in a previous blog post.</p>

<p>Lastly, for this section, I’m going to bind to the constructor for this type
(even though the constructor is almost never called directly). By convention,
this function is called <code class="language-plaintext highlighter-rouge">make</code>.</p>

<figure class="highlight"><pre><code class="language-ocaml" data-lang="ocaml"><span class="p">[</span><span class="o">@</span><span class="n">bs</span><span class="o">.</span><span class="k">new</span><span class="p">]</span> <span class="k">external</span> <span class="n">make</span> <span class="o">:</span> <span class="n">fetchEvent</span> <span class="o">=</span> <span class="s2">"FetchEvent"</span><span class="p">;</span></code></pre></figure>

<h3 id="binding-to-javascript-object-properties-in-reasonml">Binding to JavaScript object properties in ReasonML</h3>
<p>At this point, I’m just moving straight down the page of the 
<a href="https://developer.mozilla.org/en-US/docs/Web/API/FetchEvent">FetchEvent</a>
documentation, which brings me to the “Properties” section. I couldn’t find 
anything on binding properties in the 
ReasonML documentation. Fortunately, the BuckleScript
<a href="https://bucklescript.github.io/docs/en/property-access#static-property-access">documentation</a> had the syntax I needed. Basically, each property is a function
that takes an instance of my object, like so:</p>

<figure class="highlight"><pre><code class="language-ocaml" data-lang="ocaml"><span class="o">/*</span> <span class="n">properties</span> <span class="o">*/</span>
<span class="p">[</span><span class="o">@</span><span class="n">bs</span><span class="o">.</span><span class="n">get</span><span class="p">]</span> <span class="k">external</span> <span class="n">clientId</span><span class="o">:</span> <span class="n">fetchEvent</span> <span class="o">=&gt;</span> <span class="kt">string</span>  <span class="o">=</span> <span class="s2">"clientId"</span><span class="p">;</span>
<span class="p">[</span><span class="o">@</span><span class="n">bs</span><span class="o">.</span><span class="n">get</span><span class="p">]</span> <span class="k">external</span> <span class="n">preloadResponse</span><span class="o">:</span> <span class="n">fetchEvent</span> <span class="o">=&gt;</span> <span class="nn">Js</span><span class="p">.</span><span class="nn">Promise</span><span class="p">.</span><span class="n">t</span><span class="p">(</span><span class="nn">Response</span><span class="p">.</span><span class="n">t</span><span class="p">)</span> <span class="o">=</span>
  <span class="s2">"preloadResponse"</span><span class="p">;</span>
<span class="p">[</span><span class="o">@</span><span class="n">bs</span><span class="o">.</span><span class="n">get</span><span class="p">]</span> <span class="k">external</span> <span class="n">replacesClientId</span><span class="o">:</span> <span class="n">fetchEvent</span> <span class="o">=&gt;</span> <span class="kt">string</span> <span class="o">=</span> <span class="s2">"replacesClientId"</span><span class="p">;</span>
<span class="p">[</span><span class="o">@</span><span class="n">bs</span><span class="o">.</span><span class="n">get</span><span class="p">]</span> <span class="k">external</span> <span class="n">resultingClientId</span><span class="o">:</span> <span class="n">fetchEvent</span> <span class="o">=&gt;</span> <span class="kt">string</span> <span class="o">=</span>
  <span class="s2">"resultingClientId"</span><span class="p">;</span>
<span class="p">[</span><span class="o">@</span><span class="n">bs</span><span class="o">.</span><span class="n">get</span><span class="p">]</span> <span class="k">external</span> <span class="n">request</span><span class="o">:</span> <span class="n">fetchEvent</span> <span class="o">=&gt;</span> <span class="nn">Request</span><span class="p">.</span><span class="n">t</span> <span class="o">=</span> <span class="s2">"request"</span><span class="p">;</span></code></pre></figure>

<p>Note that the <code class="language-plaintext highlighter-rouge">Request.t</code> and <code class="language-plaintext highlighter-rouge">Response.t</code> types are available to us directly 
because we <code class="language-plaintext highlighter-rouge">open</code>ed <code class="language-plaintext highlighter-rouge">Fetch</code> earlier. Otherwise, we would have had to preface
those references with <code class="language-plaintext highlighter-rouge">Fetch.</code>.</p>

<h3 id="finishing-up-by-binding-methods-and-exposing-t">Finishing up by binding methods and exposing t</h3>

<p>In continuing working my way down the documentation page, I see there are just
two methods for the type, and one of them is built into the supertype
<code class="language-plaintext highlighter-rouge">ExtendableEvent</code>. We can implement this the same way we implement the 
properties, but with <code class="language-plaintext highlighter-rouge">[@bs.send]</code> to tell BuckleScript to emit a function
binding instead of a property binding.</p>

<figure class="highlight"><pre><code class="language-ocaml" data-lang="ocaml"><span class="p">[</span><span class="o">@</span><span class="n">bs</span><span class="o">.</span><span class="n">send</span><span class="p">]</span> <span class="k">external</span> <span class="n">respondWith</span><span class="o">:</span> <span class="n">fetchEvent</span> <span class="o">=&gt;</span> <span class="nn">Js</span><span class="p">.</span><span class="nn">Promise</span><span class="p">.</span><span class="n">t</span><span class="p">(</span><span class="nn">Response</span><span class="p">.</span><span class="n">t</span><span class="p">)</span> <span class="o">=</span>
  <span class="s2">"respondWith"</span><span class="p">;</span>

<span class="o">//</span> <span class="n">also</span> <span class="n">inherits</span> <span class="n">waitUntil</span> <span class="n">from</span> <span class="nn">ExtendableEvent</span><span class="p">.</span></code></pre></figure>

<p>Lastly, unlike <code class="language-plaintext highlighter-rouge">ExtendableEvent</code>, I expect my library users to reference 
<code class="language-plaintext highlighter-rouge">FetchEvent</code> directly, so let’s be nice and expose a <code class="language-plaintext highlighter-rouge">t</code> reference.</p>

<figure class="highlight"><pre><code class="language-ocaml" data-lang="ocaml"><span class="k">type</span> <span class="n">t</span> <span class="o">=</span> <span class="n">fetchEvent</span><span class="p">;</span></code></pre></figure>

<p>This makes it easy for users to reference our type as <code class="language-plaintext highlighter-rouge">FetchEvent.t</code>.</p>

<h3 id="in-conclusion">In Conclusion</h3>
<p>That’s good enough for tonight. Right now I assume I now have a usable 
<code class="language-plaintext highlighter-rouge">FetchEvent</code>, thought I still need to learn to actually test this stuff. 
I’m optimistic about this binding project. I think if I bind just another
couple of types I’ll have enough to make a real ServiceWorker in ReasonML.</p>

      </div>
    </div>
  </body>
</html>
